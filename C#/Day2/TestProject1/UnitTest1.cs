using KthElement;

namespace TestProject1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestKInArray()
        {
            // Arrange
            int k = 4;
            int[] numbers = {3,15,5,12,13};

            int expected = 15;

            // Act
            int result = Program.KthMin(k, numbers);


            // Assert
            Assert.AreEqual(expected, result);

        }

        [TestMethod]
        public void TestKOutOfArray()
        {
            // Arrange
            int k = 10;
            int[] numbers = { 3, 15, 5, 12, 13 };

            int expected = 0;

            // Act
            int result = Program.KthMin(k, numbers);


            // Assert
            Assert.AreEqual(expected, result);

        }


        [TestMethod]
        public void TestKInArrayBorder()
        {
            // Arrange
            int k = 6;
            int[] numbers = { 2, 12, 0, 29, 19, 72, 1 };

            int expected = 72;

            // Act
            int result = Program.KthMin(k, numbers);


            // Assert
            Assert.AreEqual(expected, result);

        }
    }
}