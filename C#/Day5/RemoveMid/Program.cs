﻿using System;
using System.Collections.Generic;

namespace RemoveMid
{
    public class Program
    {



        static public LinkedList<int> RemoveMid(LinkedList<int> list)
        {
            if (list == null || list.Count <= 1)
            {
                return list;
            }

            var middleIndex = list.Count / 2;
            var middleNode = list.First;

            // Get to the middle node
            for (int i = 0; i < middleIndex; i++)
            {
                middleNode = middleNode.Next;
            }

            list.Remove(middleNode);
            return list;
        }

        static void Main(string[] args)
        {


            LinkedList<int> myList = new LinkedList<int>();

            myList.AddLast(1);
            myList.AddLast(2);
            myList.AddLast(3);
            myList.AddLast(4);


            LinkedList<int> test = RemoveMid(myList);

            foreach(var item in test)
            {
                Console.WriteLine(item);
            }
            //Console.WriteLine(test);
        }
    }
}