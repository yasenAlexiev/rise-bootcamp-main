﻿namespace Task2
{
    internal class Program
    {

        public static int[] IntersectNumbers(int[]firstArr, int[] secondArr) // вярно
        {

            // Get unique  elements of the first array
            HashSet<int> set = new HashSet<int>(firstArr);

            // Future result with intersections
            List<int> list = new List<int>();

            foreach (int i in secondArr)
            {
                if (set.Contains(i))
                {
                    list.Add(i);
                    set.Remove(i);
                }
            }

            return list.ToArray();
        }

        static void Main(string[] args)
        {

            int[] a = { 1, 2, 3 };
            int[] b = { 1, 8 ,9 };

            int[] intersection = IntersectNumbers(a, b);

            Console.WriteLine(@"Intersection between  " + string.Join(",", a) + " and " + string.Join(",", b) + " is: " + string.Join(",", intersection));

        }
    }
}