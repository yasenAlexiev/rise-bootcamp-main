﻿namespace Task2___CollectCoins
{
    internal class Program
    {



        // Get all the edges from the input and transform them into a list
        public static List<(int,int)> CreateEdges(int[][] input)
        {
            List<(int, int)> edges = new List<(int, int)>();

            for (int i=1; i<input.Length; i++)
            {
                edges.Add((input[i][0], input[i][1]));
            }

                return edges;
        }



        //Create a matrix based on the connections between the nodes
        public static int[,] CreateMatrix(List<(int,int)> edges, int[][]input) {

            int numNodes = input[0][0];
            int numEdges = input[0][1];


            // Create a matrix
            int[,] matrix = new int[numNodes, numEdges];


            // Mark in the array the intersections for the undirected graph
            for(int i=0; i<numEdges; i++)
            {
                // Remove 1 because we are not starting from 0 (error)
                int node1 = edges[i].Item1-1;
                int node2 = edges[i].Item2-1;

                //Console.WriteLine(node1 + " and " +node2);

                matrix[node1, node2] = 1;
                matrix[node2, node1] = 1;
            }

            return matrix;
        }

        public static bool IsHamiltonian(int[,] matrix)
        {
            int numNodes = matrix.GetLength(0);
            int numEdges = matrix.GetLength(1);

            Console.WriteLine(numNodes);

            // Check if the number of nodes is greater than 2
            if (numNodes <= 2)
            {
                return false;
            }

            // Check if the count of each node is at least (numNodes / 2)
            for (int i = 0; i < numNodes; i++)
            {
                int count = 0;

                // Travers through each row and get the sum of the 1s
                for (int j = 0; j < numEdges; j++)
                {
                    count += matrix[i, j];
                }


                // If we have at least 1 Hamiltonian path => break and return true
                if (count > (numNodes / 2))
                {
                    return true;
                }
            }

            return false;
        }


        public static bool StartCollectingCoins(int[][] input)
        {

            // Get edges
            List<(int, int)> edges = CreateEdges(input);

            // Get matrix
            int[,] matrix = CreateMatrix(edges, input);

            // Find Hamiltonian path
            bool result = IsHamiltonian(matrix);

            return result;

        }

        static void Main(string[] args)
        {
            int[][] input1 = new int[][]
                {
                    new int[] {3, 3},
                    new int[] {1, 2},
                    new int[] {2, 3},
                    new int[] {1, 3}
                };

            int[][] input2 = new int[][]
                {
                    new int[] {3, 2},
                    new int[] {1, 2},
                    new int[] {2, 1}
                };


            int[][] input3 = new int[][]
                {
                    new int[] {5, 6},
                    new int[] {1, 2},
                    new int[] {1, 3},
                    new int[] {2, 1},
                    new int[] {2, 3},
                    new int[] {2, 4},
                    new int[] {3, 1},
                    new int[] {3, 2},
                    new int[] {3, 5},
                    new int[] {4, 2},
                    new int[] {4, 5},
                    new int[] {5, 4},
                    new int[] {5, 3},
                };

            var result = StartCollectingCoins(input3);


            Console.WriteLine("Micro can collect all the coins: " + result);
        }
    }
}