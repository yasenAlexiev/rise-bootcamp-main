﻿using System;

namespace Day7
{
   

        public class Node
        {
            public int value;
            public Node left, right;
            public Node(int item) {
                value = item;
                left = right = null;
            }
        }

        public class BinaryTree
        {
            public Node root;

            public int maxDepth(Node node)
            {
                if  (node == null)
                {
                    return 0;
                }
                else
                {
                    int lDepth = maxDepth(node.left);
                    int rDepth = maxDepth(node.right);

                    if (lDepth > rDepth) return (lDepth + 1);
                    else return (rDepth + 1);
                }
            }
        }
    public class Program
    {
        public static void Main(string[] args)
        {
            BinaryTree tree = new BinaryTree();

            tree.root = new Node(1);
            tree.root.left = new Node(4);
            tree.root.right = new Node(3);
            tree.root.left.left = new Node(4);
            tree.root.left.right = new Node(5);



            Console.WriteLine("Height: " + tree.maxDepth(tree.root));
        }
    }
}