﻿using System;

namespace Task4___KthSmallestelement
{
    class Node
    {
        public int value;
        public Node left, right;
        public Node(int x)
        {
            value = x;
            left = right = null;
        }
    }

    class GFG
    {
        static int count = 0;


        // Node recursion to insert a new node
        public static Node Insert(Node root, int x)
        {
            if (root == null) return new Node(x);
            if (x < root.value) root.left = Insert(root.left, x);
            else if (x > root.value) root.right = Insert(root.right, x);
            return root;
        }

        public static Node KthSmallestNumber(Node root, int k)
        {
            if (root == null) return null;

            Node left = KthSmallestNumber(root.left, k);


            if (left != null) return left;
            count++;

            if(count==k) return root;

            return KthSmallestNumber(root.right, k);
        }


        public static void GetKthSmallest(Node root, int k)
        {
            count = 0;
            Node res = KthSmallestNumber(root, k);

            if (res == null)
            {
                Console.WriteLine("Invalid K!");
            }
            else
            {
                Console.WriteLine("K-th Smallest Element is " + res.value);
            }
        }
    }

    internal class Program
    {
        static void Main(string[] args)
        {

            Node root = null;

            int[] nodes = { 20, 8, 22, 4, 12, 10, 14 };

            foreach (int x in nodes)
                root = GFG.Insert(root, x);

            int k = 2;
            GFG.GetKthSmallest(root, k);

            Console.WriteLine("Hello, World!");
        }
    }
}